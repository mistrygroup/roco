# ROCO - Robot Controller Interface

## Overview

A C++ library that provides a common interface for robot controllers.
This library is used by the Robot Controller Manager [rocoma](https://bitbucket.org/leggedrobotics/rocoma).

[Documentation](http://docs.leggedrobotics.com/rocoma_doc/)

The software has been tested under ROS Melodic and Ubuntu 18.04.

The source code is released under a [BSD 3-Clause license](LICENSE).

**Author(s):** Christian Gehring
Date: Dec. 2014

## Building

[![Build Status](https://ci.leggedrobotics.com/buildStatus/icon?job=bitbucket_leggedrobotics/roco/master)](https://ci.leggedrobotics.com/job/bitbucket_leggedrobotics/job/roco/job/master/)

In order to install, clone the latest version from this repository into your catkin workspace and compile the packages.

### Dependencies
* **[message_logger](https://bitbucket.org/leggedrobotics/message_logger):** Message logger

## Bugs & Feature Requests

Please report bugs and request features using the [Issue Tracker](https://github.com/ethz-asl/ros_best_practices/issues).
